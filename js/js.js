            //funções
function func(x, y) {
    return x + y;
}
//console.log(func(10, 10));


            //Operadores Lógicos
//var x = 5;
//var y = 10;

function or(){
    return x < 10 || y > 10;
}

function and(){
    return x < 10 && y == 10;
}
function not(){
    return x != 5;
}
//console.log('função retornada OR:', or());
//console.log('função retornada AND:', and());
//console.log('função retornada NOT:', not());

            //Operadores Unários

//var x = "5" + "5";
//var y = -"5";

function unario() {
    return x;
}

//console.log(unario());

            //Estrutura Léxica

            //Case Sensitive

//var Variavel = '20';
//var variavel = '10';

function lex() {
    return Variavel != variavel;
}
//console.log(lex());

            //Instruções Condicionais

//var x = 2;
//var y = 20;

function cond() {
    if(x > 5) {
        y = 30;
        alert("x é maior que 5");
    }
    else if(y == 20){
        alert("y é 20");
    }
    else {
        alert("x é menor que 5");
    }
}

//cond();
//console.log(y);

            //Tipos Primitivos e Objetos

var pessoa = {
    idade: 23,
    nome: 'Daniel',
    sobrenome: 'Friggi',
    altura: 1.78,
    casado: true,
    aniversario: function () {
        pessoa.idade++;
    },
    nomeComSobrenome: function() {
        pessoa.nome = pessoa.nome + " " + pessoa.sobrenome;
    }
};

var apresentar = function() {
    console.log(
        "O nome da pessoa é: " + pessoa.nome + " Sua idade é: " + pessoa.idade
    );
    if (pessoa.idade > 25) {
        alert("O " + pessoa.nome + " tem mais de 25 anos"); 
    }
    else {
        alert("O " + pessoa.nome + " tem menos de 25 anos"); 
    }
}

//apresentar();

            //Métodos de Objetos

pessoa.aniversario();
pessoa.nomeComSobrenome();

//console.log(pessoa.idade);
//console.log(pessoa.nome);

            //Condicional Ternário

var carro = {
    portas: 4,
    marca: 'Fiat'
}

var modelo;
var ternario = carro.portas == 4 ? (modelo = 'Quatro portas') : (modelo = 'Duas portas') ;

//console.log(ternario);

            //Retorno de Funções com Arrays e Objetos

function arr()  {
    return [1,2,3,4,5,6,7,8,9];
}

//console.log(arr()[0]);

function calc(){
    var soma = arr()[2] + arr()[2];
    return soma;
}

//console.log(calc())

function obj()  {
    return {
        peso: 89,
        humor: 'feliz'
    };
}

//console.log(obj().peso);

            //Parmâetros de Funções como Arrays e Objetos

var array = [1,2,4,5,6,6]

function ar(arg) {
    return arg;
}

function ob(arg) {
    return arg.nome;
}

//console.log(ob(pessoa));

//console.log(ar(array)[1]);

            //Virgulas e Switch

var a, b = 2, c = 3;

function virgula() {
    return (a += 1, a);
}

function swit(x) {
    switch(x) {
        case 1:
            console.log('O numero é igual a 1');
        break;
        case 2:
            console.log('O numero é igual a 2');
        break;
        default:
            console.log('O numero nao é 1 nem 2');
        break;
    }
}
//swit(2);

            //While

var contador = 0;

//while(contador < 10) {
//   console.log(contador);
//    contador++;
//}

            //Length

var len = ["Daniel", "Friggi", "Oliveira", {pess: "29 anos"}];
var quant = len.length;

//console.log('Essa é a quantidade: ', quant);

            //Push

function attArr() {
    len.push('String');
    return len;
}

//console.log(attArr());

            //IIFE (Função auto-executavel)

//(function () {
//   console.log( 40+40);
//})();

            //Template String

var n = "Daniel";
document.write(`<h1>${typeof n}</h1>`);

